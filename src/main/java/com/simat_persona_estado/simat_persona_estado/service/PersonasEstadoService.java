package com.simat_persona_estado.simat_persona_estado.service;

import com.simat_persona_estado.simat_persona_estado.entity.PersonasEstadoEntity;

public interface PersonasEstadoService {

	public PersonasEstadoEntity getPersonasEstado(String idPersona);
}
