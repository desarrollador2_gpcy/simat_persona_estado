package com.simat_persona_estado.simat_persona_estado.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simat_persona_estado.simat_persona_estado.entity.PersonasEstadoEntity;
import com.simat_persona_estado.simat_persona_estado.repository.PersonasEstadoRepository;
import com.simat_persona_estado.simat_persona_estado.service.PersonasEstadoService;

@Service
public class PersonasEstadoServiceImpl implements PersonasEstadoService{

	
	@Autowired
	private PersonasEstadoRepository personasEstadoRepository;

	public PersonasEstadoEntity getPersonasEstado(String idPersona) {
		return personasEstadoRepository.getPersonasEstado(idPersona);
	}
}
