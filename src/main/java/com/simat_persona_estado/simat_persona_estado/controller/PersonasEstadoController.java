package com.simat_persona_estado.simat_persona_estado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.simat_persona_estado.simat_persona_estado.entity.PersonasEstadoEntity;
import com.simat_persona_estado.simat_persona_estado.service.PersonasEstadoService;


@RestController
@RequestMapping("/personasEstado")
public class PersonasEstadoController {
	
	
	@Autowired
	PersonasEstadoService personasEstadoService;
	

	@RequestMapping(value = "/getPersonas/estado/{idPersona}", method = RequestMethod.GET)
	public ResponseEntity<PersonasEstadoEntity> getPersonaEstado(@PathVariable("idPersona") String idPersona) {
		try {
			PersonasEstadoEntity personaEntity = personasEstadoService.getPersonasEstado(idPersona);
			return new ResponseEntity<PersonasEstadoEntity>(personaEntity, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
