package com.simat_persona_estado.simat_persona_estado.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.simat_persona_estado.simat_persona_estado.entity.PersonasEstadoEntity;


@Repository
public interface PersonasEstadoRepository extends JpaRepository<PersonasEstadoEntity, Integer> {
	
	@Query(value = "SELECT ea.*"
			+ " FROM PERSONAS P, ESTADO_ALUMNO EA"
			+ " WHERE P.ESTADO_ID=EA.ID"
			+ " AND P.ID=:idPersona", nativeQuery = true)
	PersonasEstadoEntity getPersonasEstado(@Param("idPersona")String idPersona);
}